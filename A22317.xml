<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A22317">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>By the King. A proclamation for ambassadours and forreigne ministers</title>
    <title>Proclamations. 1624-03-08</title>
    <author>England and Wales. Sovereign (1603-1625 : James I)</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A22317 of text S100908 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (STC 8723). Textual changes  and metadata enrichments aim at making the                             text more  computationally tractable, easier to read, and suitable for network-based collaborative                              curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in                             a standardized format that preserves archaic forms ('loveth', 'seekest').                              Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 3 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">A22317</idno>
    <idno type="STC">STC 8723</idno>
    <idno type="STC">ESTC S100908</idno>
    <idno type="EEBO-CITATION">99836735</idno>
    <idno type="PROQUEST">99836735</idno>
    <idno type="VID">1021</idno>
    <idno type="PROQUESTGOID">2264211862</idno>
    <availability>
     <p>This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. Searching, reading, printing, or downloading EEBO-TCP texts is reserved for the authorized users of these project partner institutions. Permission must be granted for subsequent distribution, in print or electronically, of this EEBO-TCP Phase II text, in whole or in part.</p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 2, no. A22317)</note>
    <note>Transcribed from: (Early English Books Online ; image set 1021)</note>
    <note>Images scanned from microfilm: (Early English books, 1475-1640 ; 1601:52)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>By the King. A proclamation for ambassadours and forreigne ministers</title>
      <title>Proclamations. 1624-03-08</title>
      <author>England and Wales. Sovereign (1603-1625 : James I)</author>
      <author>James I, King of England, 1566-1625.</author>
     </titleStmt>
     <extent>1 sheet ([1] p.)</extent>
     <publicationStmt>
      <publisher>by Bonham Norton and Iohn Bill, printers to the Kings most excellent Maiestie,</publisher>
      <pubPlace>Imprinted at London :</pubPlace>
      <date>M.DC.XXIII. [1623, i.e. 1624]</date>
     </publicationStmt>
     <notesStmt>
      <note>"The King, considering the respect due to Amassadors, Agents, and Public Ministers, warns all persons to forbear to use any insolency, misbehaviour, incivility, disgrace, or affront to them or their followers, but to pay them fitting reverence and courtesy, on pain, &amp;c." -- Steele.</note>
      <note>Dated at end: Whitehall, the eighth day of March, in the one and twentieth yeere of our reigne ... .</note>
      <note>The year date is given according to Lady Day dating.</note>
      <note>Steele notation: great onely according; Arms 14.</note>
      <note>Reproduction of the originals in the Henry E. Huntington Library and Art Gallery (Early English books) and the British Library (Misc. Brit. tracts).</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Diplomatic privileges and immunities -- Early works to 1800.</term>
     <term>Diplomatic and consular service -- Great Britain -- Law and legislation -- Early works to 1800.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>By the King. A proclamation for ambassadours and forreigne ministers.</ep:title>
    <ep:author>England and Wales. Sovereign </ep:author>
    <ep:publicationYear>1624</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>454</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2008-08</date>
    <label>TCP</label>
        Assigned for keying and markup
      </change>
   <change>
    <date>2008-11</date>
    <label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change>
    <date>2009-04</date>
    <label>Mona Logarbo</label>
        Sampled and proofread
      </change>
   <change>
    <date>2009-04</date>
    <label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
   <change>
    <date>2009-09</date>
    <label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A22317-e10010">
  <body xml:id="A22317-e10020">
   <div type="royal_proclamation" xml:id="A22317-e10030">
    <pb facs="tcp:1021:1" xml:id="A22317-001-a"/>
    <head xml:id="A22317-e10040">
     <figure xml:id="A22317-e10050">
      <p xml:id="A22317-e10060">
       <w lemma="i" pos="sy" xml:id="A22317-001-a-0010">I</w>
       <w lemma="r" pos="sy" xml:id="A22317-001-a-0020">R</w>
      </p>
      <p xml:id="A22317-e10070">
       <w lemma="n/a" pos="ffr" xml:id="A22317-001-a-0030">DIEV</w>
       <w lemma="n/a" pos="ffr" xml:id="A22317-001-a-0040">ET</w>
       <w lemma="n/a" pos="ffr" xml:id="A22317-001-a-0050">MON</w>
       <w lemma="n/a" pos="ffr" xml:id="A22317-001-a-0060">DROIT</w>
      </p>
      <p xml:id="A22317-e10080">
       <w lemma="n/a" pos="ffr" xml:id="A22317-001-a-0070">HONI</w>
       <w lemma="n/a" pos="ffr" xml:id="A22317-001-a-0080">SOIT</w>
       <w lemma="n/a" pos="ffr" xml:id="A22317-001-a-0090">QVI</w>
       <w lemma="n/a" pos="ffr" xml:id="A22317-001-a-0100">MAL</w>
       <w lemma="n/a" pos="ffr" xml:id="A22317-001-a-0110">Y</w>
       <w lemma="n/a" pos="ffr" xml:id="A22317-001-a-0120">PENSE</w>
      </p>
      <figDesc xml:id="A22317-e10090">royal blazon or coat of arms</figDesc>
     </figure>
    </head>
    <head xml:id="A22317-e10100">
     <w lemma="by" pos="acp" xml:id="A22317-001-a-0130">By</w>
     <w lemma="the" pos="d" xml:id="A22317-001-a-0140">the</w>
     <w lemma="King" pos="n1" xml:id="A22317-001-a-0150">King</w>
     <pc unit="sentence" xml:id="A22317-001-a-0151">.</pc>
    </head>
    <head type="sub" xml:id="A22317-e10110">
     <w lemma="❧" pos="sy" xml:id="A22317-001-a-0170">❧</w>
     <w lemma="a" pos="sy" xml:id="A22317-001-a-0180">A</w>
     <w lemma="proclamation" pos="n1" xml:id="A22317-001-a-0190">Proclamation</w>
     <w lemma="for" pos="acp" xml:id="A22317-001-a-0200">for</w>
     <w lemma="ambassador" pos="n2" reg="Ambassadors" xml:id="A22317-001-a-0210">Ambassadours</w>
     <w lemma="and" pos="cc" xml:id="A22317-001-a-0220">and</w>
     <w lemma="foreign" pos="j" reg="foreign" xml:id="A22317-001-a-0230">forreigne</w>
     <w lemma="minister" pos="n2" xml:id="A22317-001-a-0240">Ministers</w>
     <pc unit="sentence" xml:id="A22317-001-a-0250">.</pc>
    </head>
    <p xml:id="A22317-e10120">
     <hi xml:id="A22317-e10130">
      <w lemma="the" pos="d" rend="decorinit" xml:id="A22317-001-a-0260">THE</w>
      <w lemma="king" pos="n2" xml:id="A22317-001-a-0270">Kings</w>
      <w lemma="most" pos="avs-d" xml:id="A22317-001-a-0280">most</w>
      <w lemma="excellent" pos="j" xml:id="A22317-001-a-0290">Excellent</w>
      <w lemma="majesty" pos="n1" reg="Majesty" xml:id="A22317-001-a-0300">Maiestie</w>
      <pc xml:id="A22317-001-a-0310">,</pc>
      <w lemma="consider" pos="vvg" xml:id="A22317-001-a-0320">considering</w>
      <w lemma="the" pos="d" xml:id="A22317-001-a-0330">the</w>
      <w lemma="great" pos="j" xml:id="A22317-001-a-0340">great</w>
     </hi>
     <w lemma="privilege" pos="n2" reg="Privileges" xml:id="A22317-001-a-0350">Priuiledges</w>
     <pc xml:id="A22317-001-a-0360">,</pc>
     <w lemma="which" pos="crq" xml:id="A22317-001-a-0370">which</w>
     <w lemma="by" pos="acp" xml:id="A22317-001-a-0380">by</w>
     <w lemma="the" pos="d" xml:id="A22317-001-a-0390">the</w>
     <w lemma="law" pos="n2" reg="Laws" xml:id="A22317-001-a-0400">Lawes</w>
     <w lemma="of" pos="acp" xml:id="A22317-001-a-0410">of</w>
     <w lemma="GOD" pos="nn1" xml:id="A22317-001-a-0420">GOD</w>
     <w lemma="and" pos="cc" xml:id="A22317-001-a-0430">and</w>
     <w lemma="nation" pos="n2" xml:id="A22317-001-a-0440">Nations</w>
     <w lemma="be" pos="vvb" xml:id="A22317-001-a-0450">are</w>
     <w lemma="attribute" pos="vvn" xml:id="A22317-001-a-0460">attributed</w>
     <w lemma="unto" pos="acp" reg="unto" xml:id="A22317-001-a-0470">vnto</w>
     <w lemma="the" pos="d" xml:id="A22317-001-a-0480">the</w>
     <w lemma="person" pos="n2" xml:id="A22317-001-a-0490">persons</w>
     <w lemma="of" pos="acp" xml:id="A22317-001-a-0500">of</w>
     <w lemma="ambassador" pos="n2" xml:id="A22317-001-a-0510">Ambassadors</w>
     <pc xml:id="A22317-001-a-0520">,</pc>
     <w lemma="agent" pos="n2" xml:id="A22317-001-a-0530">Agents</w>
     <pc xml:id="A22317-001-a-0540">,</pc>
     <w lemma="and" pos="cc" xml:id="A22317-001-a-0550">and</w>
     <w lemma="public" pos="j" reg="public" xml:id="A22317-001-a-0560">publique</w>
     <w lemma="minister" pos="n2" xml:id="A22317-001-a-0570">Ministers</w>
     <w lemma="of" pos="acp" xml:id="A22317-001-a-0580">of</w>
     <w lemma="foreign" pos="j" reg="foreign" xml:id="A22317-001-a-0590">forreigne</w>
     <w lemma="prince" pos="n2" xml:id="A22317-001-a-0600">Princes</w>
     <w lemma="and" pos="cc" xml:id="A22317-001-a-0610">and</w>
     <w lemma="state" pos="n2" xml:id="A22317-001-a-0620">States</w>
     <pc xml:id="A22317-001-a-0630">,</pc>
     <w lemma="and" pos="cc" xml:id="A22317-001-a-0640">and</w>
     <w lemma="the" pos="d" xml:id="A22317-001-a-0650">the</w>
     <w lemma="great" pos="j" xml:id="A22317-001-a-0660">great</w>
     <w lemma="reverence" pos="n1" reg="Reverence" xml:id="A22317-001-a-0670">Reuerence</w>
     <w lemma="and" pos="cc" xml:id="A22317-001-a-0680">and</w>
     <w lemma="respect" pos="n1" xml:id="A22317-001-a-0690">Respect</w>
     <pc xml:id="A22317-001-a-0700">,</pc>
     <w lemma="which" pos="crq" xml:id="A22317-001-a-0710">which</w>
     <w lemma="of" pos="acp" xml:id="A22317-001-a-0720">of</w>
     <w lemma="right" pos="n1-j" xml:id="A22317-001-a-0730">right</w>
     <w lemma="appertain" pos="vvz" xml:id="A22317-001-a-0740">appertaineth</w>
     <w lemma="unto" pos="acp" reg="unto" xml:id="A22317-001-a-0750">vnto</w>
     <w lemma="they" pos="pno" xml:id="A22317-001-a-0760">them</w>
     <pc xml:id="A22317-001-a-0770">,</pc>
     <w lemma="and" pos="cc" xml:id="A22317-001-a-0780">and</w>
     <w lemma="weigh" pos="vvg" xml:id="A22317-001-a-0790">weighing</w>
     <w lemma="the" pos="d" xml:id="A22317-001-a-0800">the</w>
     <w lemma="manifold" pos="j" xml:id="A22317-001-a-0810">manifold</w>
     <w lemma="and" pos="cc" xml:id="A22317-001-a-0820">and</w>
     <w lemma="important" pos="j" xml:id="A22317-001-a-0830">important</w>
     <w lemma="reason" pos="n2" xml:id="A22317-001-a-0840">Reasons</w>
     <w lemma="thereof" pos="av" xml:id="A22317-001-a-0850">thereof</w>
     <pc xml:id="A22317-001-a-0860">,</pc>
     <w lemma="and" pos="cc" xml:id="A22317-001-a-0870">and</w>
     <w lemma="how" pos="crq" xml:id="A22317-001-a-0880">how</w>
     <w lemma="necere" pos="av-j" xml:id="A22317-001-a-0890">necerely</w>
     <w lemma="every" pos="d" reg="every" xml:id="A22317-001-a-0900">euery</w>
     <w lemma="small" pos="j" xml:id="A22317-001-a-0910">small</w>
     <w lemma="neglect" pos="n1" xml:id="A22317-001-a-0920">neglect</w>
     <w lemma="and" pos="cc" xml:id="A22317-001-a-0930">and</w>
     <w lemma="disrespect" pos="n1" xml:id="A22317-001-a-0940">disrespect</w>
     <w lemma="of" pos="acp" xml:id="A22317-001-a-0950">of</w>
     <w lemma="they" pos="pno" xml:id="A22317-001-a-0960">them</w>
     <pc xml:id="A22317-001-a-0970">,</pc>
     <w lemma="touch" pos="vvz" xml:id="A22317-001-a-0980">toucheth</w>
     <w lemma="not" pos="xx" xml:id="A22317-001-a-0990">not</w>
     <w lemma="only" pos="av-j" reg="only" xml:id="A22317-001-a-1000">onely</w>
     <w lemma="those" pos="d" xml:id="A22317-001-a-1010">those</w>
     <w lemma="prince" pos="n2" xml:id="A22317-001-a-1020">Princes</w>
     <w lemma="and" pos="cc" xml:id="A22317-001-a-1030">and</w>
     <w lemma="state" pos="n2" xml:id="A22317-001-a-1040">States</w>
     <w lemma="by" pos="acp" xml:id="A22317-001-a-1050">by</w>
     <w lemma="who" pos="crq" xml:id="A22317-001-a-1060">whom</w>
     <w lemma="they" pos="pns" xml:id="A22317-001-a-1070">they</w>
     <w lemma="be" pos="vvb" xml:id="A22317-001-a-1080">are</w>
     <w lemma="employ" pos="vvn" reg="employed" xml:id="A22317-001-a-1090">imployed</w>
     <pc xml:id="A22317-001-a-1100">,</pc>
     <w lemma="but" pos="acp" xml:id="A22317-001-a-1110">but</w>
     <w lemma="all" pos="d" xml:id="A22317-001-a-1120">all</w>
     <w lemma="other" pos="pi2-d" xml:id="A22317-001-a-1130">others</w>
     <w lemma="in" pos="acp" xml:id="A22317-001-a-1140">in</w>
     <w lemma="example" pos="n1" xml:id="A22317-001-a-1150">example</w>
     <w lemma="and" pos="cc" xml:id="A22317-001-a-1160">and</w>
     <w lemma="consequence" pos="n1" xml:id="A22317-001-a-1170">consequence</w>
     <pc xml:id="A22317-001-a-1180">,</pc>
     <w lemma="and" pos="cc" xml:id="A22317-001-a-1190">and</w>
     <w lemma="as" pos="acp" xml:id="A22317-001-a-1200">as</w>
     <w lemma="a" pos="d" xml:id="A22317-001-a-1210">a</w>
     <w lemma="necessary" pos="j" reg="necessary" xml:id="A22317-001-a-1220">necessarie</w>
     <w lemma="dependent" pos="j" xml:id="A22317-001-a-1230">dependant</w>
     <w lemma="thereupon" pos="av" xml:id="A22317-001-a-1240">thereupon</w>
     <pc xml:id="A22317-001-a-1250">,</pc>
     <w lemma="the" pos="d" xml:id="A22317-001-a-1260">the</w>
     <w lemma="universal" pos="j" reg="universal" xml:id="A22317-001-a-1270">vniuersall</w>
     <w lemma="weal" pos="n1" reg="weal" xml:id="A22317-001-a-1280">weale</w>
     <w lemma="and" pos="cc" xml:id="A22317-001-a-1290">and</w>
     <w lemma="tranquillity" pos="n1" reg="tranquillity" xml:id="A22317-001-a-1300">tranquillitie</w>
     <w lemma="of" pos="acp" xml:id="A22317-001-a-1310">of</w>
     <w lemma="all" pos="d" xml:id="A22317-001-a-1320">all</w>
     <w lemma="kingdom" pos="n2" reg="Kingdoms" xml:id="A22317-001-a-1330">Kingdomes</w>
     <w lemma="and" pos="cc" xml:id="A22317-001-a-1340">and</w>
     <w lemma="state" pos="n2" xml:id="A22317-001-a-1350">States</w>
     <pc xml:id="A22317-001-a-1360">,</pc>
     <w lemma="have" pos="vvg" reg="having" xml:id="A22317-001-a-1370">hauing</w>
     <w lemma="intercourse" pos="n1" reg="intercourse" xml:id="A22317-001-a-1380">entercourse</w>
     <pc xml:id="A22317-001-a-1390">,</pc>
     <w lemma="commerce" pos="n1" xml:id="A22317-001-a-1400">commerce</w>
     <pc xml:id="A22317-001-a-1410">,</pc>
     <w lemma="or" pos="cc" xml:id="A22317-001-a-1420">or</w>
     <w lemma="correspondence" pos="n1" xml:id="A22317-001-a-1430">correspondence</w>
     <w lemma="together" pos="av" xml:id="A22317-001-a-1440">together</w>
     <pc xml:id="A22317-001-a-1450">;</pc>
     <w lemma="and" pos="cc" xml:id="A22317-001-a-1460">And</w>
     <w lemma="will" pos="vvg" xml:id="A22317-001-a-1470">willing</w>
     <w lemma="that" pos="cs" xml:id="A22317-001-a-1480">that</w>
     <w lemma="as" pos="acp" xml:id="A22317-001-a-1490">as</w>
     <w lemma="great" pos="j" xml:id="A22317-001-a-1500">great</w>
     <w lemma="observance" pos="n1" reg="observance" xml:id="A22317-001-a-1510">obseruance</w>
     <w lemma="be" pos="vvi" xml:id="A22317-001-a-1520">be</w>
     <w lemma="use" pos="vvn" reg="used" xml:id="A22317-001-a-1530">vsed</w>
     <w lemma="towards" pos="acp" xml:id="A22317-001-a-1540">towards</w>
     <w lemma="they" pos="pno" xml:id="A22317-001-a-1550">them</w>
     <w lemma="within" pos="acp" xml:id="A22317-001-a-1560">within</w>
     <w lemma="this" pos="d" xml:id="A22317-001-a-1570">this</w>
     <w lemma="his" pos="po" xml:id="A22317-001-a-1580">His</w>
     <w lemma="majesty" pos="ng1" reg="Majesty's" xml:id="A22317-001-a-1590">Maiesties</w>
     <w lemma="realm" pos="n1" reg="Realm" xml:id="A22317-001-a-1600">Realme</w>
     <pc xml:id="A22317-001-a-1610">,</pc>
     <w lemma="as" pos="acp" xml:id="A22317-001-a-1620">as</w>
     <w lemma="in" pos="acp" xml:id="A22317-001-a-1630">in</w>
     <w lemma="other" pos="d" xml:id="A22317-001-a-1640">other</w>
     <w lemma="the" pos="d" xml:id="A22317-001-a-1650">the</w>
     <w lemma="best" pos="js" xml:id="A22317-001-a-1660">best</w>
     <w lemma="govern" pos="vvn" reg="governed" xml:id="A22317-001-a-1670">gouerned</w>
     <w lemma="kingdom" pos="n2" reg="Kingdoms" xml:id="A22317-001-a-1680">Kingdomes</w>
     <w lemma="and" pos="cc" xml:id="A22317-001-a-1690">and</w>
     <w lemma="nation" pos="n2" xml:id="A22317-001-a-1700">Nations</w>
     <pc xml:id="A22317-001-a-1710">,</pc>
     <w lemma="have" pos="vvz" xml:id="A22317-001-a-1720">Hath</w>
     <w lemma="think" pos="vvn" xml:id="A22317-001-a-1730">thought</w>
     <w lemma="fit" pos="j" xml:id="A22317-001-a-1740">fit</w>
     <w lemma="hereby" pos="av" xml:id="A22317-001-a-1750">hereby</w>
     <w lemma="to" pos="prt" xml:id="A22317-001-a-1760">to</w>
     <w lemma="forewarn" pos="vvi" reg="forewarn" xml:id="A22317-001-a-1770">forewarne</w>
     <w lemma="all" pos="d" xml:id="A22317-001-a-1780">all</w>
     <w lemma="person" pos="n2" xml:id="A22317-001-a-1790">persons</w>
     <w lemma="whatsoever" pos="crq" reg="whatsoever" xml:id="A22317-001-a-1800">whatsoeuer</w>
     <pc xml:id="A22317-001-a-1810">,</pc>
     <w lemma="that" pos="cs" xml:id="A22317-001-a-1820">That</w>
     <w lemma="they" pos="pns" xml:id="A22317-001-a-1830">they</w>
     <w lemma="not" pos="xx" xml:id="A22317-001-a-1840">not</w>
     <w lemma="only" pos="av-j" reg="only" xml:id="A22317-001-a-1850">onely</w>
     <w lemma="forbear" pos="vvi" reg="forbear" xml:id="A22317-001-a-1860">forbeare</w>
     <w lemma="to" pos="prt" xml:id="A22317-001-a-1870">to</w>
     <w lemma="use" pos="vvi" reg="use" xml:id="A22317-001-a-1880">vse</w>
     <w lemma="any" pos="d" xml:id="A22317-001-a-1890">any</w>
     <w lemma="insolency" pos="n1" reg="insolency" xml:id="A22317-001-a-1900">insolencie</w>
     <pc xml:id="A22317-001-a-1910">,</pc>
     <w lemma="misbehaviour" pos="n1" reg="misbehaviour" xml:id="A22317-001-a-1920">misbehauiour</w>
     <pc xml:id="A22317-001-a-1930">,</pc>
     <w lemma="incivility" pos="n1" reg="incivility" xml:id="A22317-001-a-1940">inciuilitie</w>
     <pc xml:id="A22317-001-a-1950">,</pc>
     <w lemma="disgrace" pos="n1" xml:id="A22317-001-a-1960">disgrace</w>
     <pc xml:id="A22317-001-a-1970">,</pc>
     <w lemma="or" pos="cc" xml:id="A22317-001-a-1980">or</w>
     <w lemma="affront" pos="n1" xml:id="A22317-001-a-1990">affront</w>
     <pc xml:id="A22317-001-a-2000">,</pc>
     <w lemma="unto" pos="acp" reg="unto" xml:id="A22317-001-a-2010">vnto</w>
     <w lemma="any" pos="d" xml:id="A22317-001-a-2020">any</w>
     <w lemma="ambassador" pos="n2" xml:id="A22317-001-a-2030">Ambassadors</w>
     <pc xml:id="A22317-001-a-2040">,</pc>
     <w lemma="agent" pos="n2" xml:id="A22317-001-a-2050">Agents</w>
     <pc xml:id="A22317-001-a-2060">,</pc>
     <w lemma="or" pos="cc" xml:id="A22317-001-a-2070">or</w>
     <w lemma="public" pos="j" reg="public" xml:id="A22317-001-a-2080">publique</w>
     <w lemma="minister" pos="n2" xml:id="A22317-001-a-2090">Ministers</w>
     <w lemma="of" pos="acp" xml:id="A22317-001-a-2100">of</w>
     <w lemma="any" pos="d" xml:id="A22317-001-a-2110">any</w>
     <w lemma="foreign" pos="j" reg="foreign" xml:id="A22317-001-a-2120">forreigne</w>
     <w lemma="prince" pos="n2" xml:id="A22317-001-a-2130">Princes</w>
     <w lemma="or" pos="cc" xml:id="A22317-001-a-2140">or</w>
     <w lemma="state" pos="n2" xml:id="A22317-001-a-2150">States</w>
     <pc xml:id="A22317-001-a-2160">,</pc>
     <w lemma="and" pos="cc" xml:id="A22317-001-a-2170">and</w>
     <w lemma="their" pos="po" xml:id="A22317-001-a-2180">their</w>
     <w lemma="follower" pos="n2" xml:id="A22317-001-a-2190">followers</w>
     <pc xml:id="A22317-001-a-2200">,</pc>
     <w lemma="servant" pos="n2" reg="servants" xml:id="A22317-001-a-2210">seruants</w>
     <pc xml:id="A22317-001-a-2220">,</pc>
     <w lemma="and" pos="cc" xml:id="A22317-001-a-2230">and</w>
     <w lemma="attendant" pos="n2-j" xml:id="A22317-001-a-2240">attendants</w>
     <pc xml:id="A22317-001-a-2250">,</pc>
     <w lemma="but" pos="acp" xml:id="A22317-001-a-2260">but</w>
     <w lemma="also" pos="av" xml:id="A22317-001-a-2270">also</w>
     <w lemma="do" pos="vvb" reg="do" xml:id="A22317-001-a-2280">doe</w>
     <w lemma="upon" pos="acp" reg="upon" xml:id="A22317-001-a-2290">vpon</w>
     <w lemma="all" pos="d" xml:id="A22317-001-a-2300">all</w>
     <w lemma="occasion" pos="n2" xml:id="A22317-001-a-2310">occasions</w>
     <w lemma="perform" pos="vvi" reg="perform" xml:id="A22317-001-a-2320">performe</w>
     <w lemma="unto" pos="acp" reg="unto" xml:id="A22317-001-a-2330">vnto</w>
     <w lemma="they" pos="pno" xml:id="A22317-001-a-2340">them</w>
     <pc xml:id="A22317-001-a-2350">,</pc>
     <w lemma="all" pos="d" xml:id="A22317-001-a-2360">all</w>
     <w lemma="such" pos="d" xml:id="A22317-001-a-2370">such</w>
     <w lemma="reverence" pos="n1" reg="reverence" xml:id="A22317-001-a-2380">reuerence</w>
     <pc xml:id="A22317-001-a-2390">,</pc>
     <w lemma="respect" pos="n1" xml:id="A22317-001-a-2400">respect</w>
     <w lemma="and" pos="cc" xml:id="A22317-001-a-2410">and</w>
     <w lemma="courtesy" pos="n1" reg="courtesy" xml:id="A22317-001-a-2420">courtesie</w>
     <pc xml:id="A22317-001-a-2430">,</pc>
     <w lemma="both" pos="av-d" xml:id="A22317-001-a-2440">both</w>
     <w lemma="in" pos="acp" xml:id="A22317-001-a-2450">in</w>
     <w lemma="speech" pos="n1" xml:id="A22317-001-a-2460">speech</w>
     <pc xml:id="A22317-001-a-2470">,</pc>
     <w lemma="gesture" pos="n1" xml:id="A22317-001-a-2480">gesture</w>
     <w lemma="and" pos="cc" xml:id="A22317-001-a-2490">and</w>
     <w lemma="otherwise" pos="av" xml:id="A22317-001-a-2500">otherwise</w>
     <pc xml:id="A22317-001-a-2510">,</pc>
     <w lemma="as" pos="acp" xml:id="A22317-001-a-2520">as</w>
     <w lemma="be" pos="vvz" xml:id="A22317-001-a-2530">is</w>
     <w lemma="most" pos="ds" xml:id="A22317-001-a-2540">most</w>
     <w lemma="fit" pos="vvg" xml:id="A22317-001-a-2550">fitting</w>
     <w lemma="to" pos="prt" xml:id="A22317-001-a-2560">to</w>
     <w lemma="proceed" pos="vvi" xml:id="A22317-001-a-2570">proceed</w>
     <w lemma="from" pos="acp" xml:id="A22317-001-a-2580">from</w>
     <w lemma="person" pos="n2" xml:id="A22317-001-a-2590">persons</w>
     <w lemma="of" pos="acp" xml:id="A22317-001-a-2600">of</w>
     <w lemma="civil" pos="j" reg="civil" xml:id="A22317-001-a-2610">ciuill</w>
     <w lemma="behaviour" pos="n1" reg="behaviour" xml:id="A22317-001-a-2620">behauiour</w>
     <pc xml:id="A22317-001-a-2630">,</pc>
     <w lemma="unto" pos="acp" reg="unto" xml:id="A22317-001-a-2640">vnto</w>
     <w lemma="man" pos="n2" xml:id="A22317-001-a-2650">men</w>
     <w lemma="of" pos="acp" xml:id="A22317-001-a-2660">of</w>
     <w lemma="that" pos="d" xml:id="A22317-001-a-2670">that</w>
     <w lemma="eminency" pos="n1" reg="eminency" xml:id="A22317-001-a-2680">eminencie</w>
     <w lemma="for" pos="acp" xml:id="A22317-001-a-2690">for</w>
     <w lemma="place" pos="n1" xml:id="A22317-001-a-2700">place</w>
     <w lemma="and" pos="cc" xml:id="A22317-001-a-2710">and</w>
     <w lemma="employment" pos="n1" xml:id="A22317-001-a-2720">employment</w>
     <pc xml:id="A22317-001-a-2730">,</pc>
     <w lemma="upon" pos="acp" reg="upon" xml:id="A22317-001-a-2740">vpon</w>
     <w lemma="pain" pos="n1" reg="pain" xml:id="A22317-001-a-2750">paine</w>
     <w lemma="of" pos="acp" xml:id="A22317-001-a-2760">of</w>
     <w lemma="his" pos="po" xml:id="A22317-001-a-2770">His</w>
     <w lemma="majesty" pos="ng1" reg="Majesty's" xml:id="A22317-001-a-2780">Maiesties</w>
     <w lemma="high" pos="js" xml:id="A22317-001-a-2790">highest</w>
     <w lemma="indignation" pos="n1" xml:id="A22317-001-a-2800">indignation</w>
     <w lemma="and" pos="cc" xml:id="A22317-001-a-2810">and</w>
     <w lemma="displeasure" pos="n1" xml:id="A22317-001-a-2820">displeasure</w>
     <pc xml:id="A22317-001-a-2830">,</pc>
     <w lemma="and" pos="cc" xml:id="A22317-001-a-2840">and</w>
     <w lemma="such" pos="d" xml:id="A22317-001-a-2850">such</w>
     <w lemma="n/a" pos="fla" xml:id="A22317-001-a-2860">senere</w>
     <w lemma="punishment" pos="n1" xml:id="A22317-001-a-2870">punishment</w>
     <w lemma="as" pos="acp" xml:id="A22317-001-a-2880">as</w>
     <w lemma="the" pos="d" xml:id="A22317-001-a-2890">the</w>
     <w lemma="offender" pos="n2" xml:id="A22317-001-a-2900">offenders</w>
     <w lemma="of" pos="acp" xml:id="A22317-001-a-2910">of</w>
     <w lemma="this" pos="d" xml:id="A22317-001-a-2920">this</w>
     <w lemma="his" pos="po" xml:id="A22317-001-a-2930">His</w>
     <w lemma="royal" pos="j" reg="Royal" xml:id="A22317-001-a-2940">Royall</w>
     <w lemma="command" pos="n1" xml:id="A22317-001-a-2950">Command</w>
     <w lemma="may" pos="vmb" xml:id="A22317-001-a-2960">may</w>
     <w lemma="deserve" pos="vvi" reg="deserve" xml:id="A22317-001-a-2970">deserue</w>
     <pc unit="sentence" xml:id="A22317-001-a-2980">.</pc>
     <w lemma="and" pos="cc" xml:id="A22317-001-a-2990">And</w>
     <w lemma="his" pos="po" xml:id="A22317-001-a-3000">His</w>
     <w lemma="majesty" pos="n1" reg="Majesty" xml:id="A22317-001-a-3010">Maiestie</w>
     <w lemma="do" pos="vvz" reg="doth" xml:id="A22317-001-a-3020">doeth</w>
     <w lemma="hereby" pos="av" xml:id="A22317-001-a-3030">hereby</w>
     <w lemma="charge" pos="n1" xml:id="A22317-001-a-3040">charge</w>
     <w lemma="and" pos="cc" xml:id="A22317-001-a-3050">and</w>
     <w lemma="command" pos="vvi" xml:id="A22317-001-a-3060">command</w>
     <pc xml:id="A22317-001-a-3070">,</pc>
     <w lemma="not" pos="xx" xml:id="A22317-001-a-3080">not</w>
     <w lemma="only" pos="av-j" reg="only" xml:id="A22317-001-a-3090">onely</w>
     <w lemma="the" pos="d" xml:id="A22317-001-a-3100">the</w>
     <w lemma="lord" pos="n2" xml:id="A22317-001-a-3110">Lords</w>
     <w lemma="of" pos="acp" xml:id="A22317-001-a-3120">of</w>
     <w lemma="his" pos="po" xml:id="A22317-001-a-3130">His</w>
     <w lemma="majesty" pos="ng1" reg="Majesty's" xml:id="A22317-001-a-3140">Maiesties</w>
     <w lemma="privy" pos="j" reg="Privy" xml:id="A22317-001-a-3150">Priuie</w>
     <w lemma="council" pos="n1" reg="Council" xml:id="A22317-001-a-3160">Councell</w>
     <pc xml:id="A22317-001-a-3170">,</pc>
     <w lemma="but" pos="acp" xml:id="A22317-001-a-3180">but</w>
     <w lemma="also" pos="av" xml:id="A22317-001-a-3190">also</w>
     <w lemma="all" pos="d" xml:id="A22317-001-a-3200">all</w>
     <w lemma="major" pos="n2" reg="Majors" xml:id="A22317-001-a-3210">Maiors</w>
     <pc xml:id="A22317-001-a-3220">,</pc>
     <w lemma="sheriff" pos="n2" reg="Sheriffs" xml:id="A22317-001-a-3230">Sheriffes</w>
     <pc xml:id="A22317-001-a-3240">,</pc>
     <w lemma="justice" pos="n2" reg="justices" xml:id="A22317-001-a-3250">Iustices</w>
     <w lemma="of" pos="acp" xml:id="A22317-001-a-3260">of</w>
     <w lemma="peace" pos="n1" xml:id="A22317-001-a-3270">Peace</w>
     <pc xml:id="A22317-001-a-3280">,</pc>
     <w lemma="and" pos="cc" xml:id="A22317-001-a-3290">and</w>
     <w lemma="all" pos="d" xml:id="A22317-001-a-3300">all</w>
     <w lemma="other" pos="pi-d" xml:id="A22317-001-a-3310">other</w>
     <w lemma="his" pos="po" xml:id="A22317-001-a-3320">His</w>
     <w lemma="majesty" pos="ng1" reg="Majesty's" xml:id="A22317-001-a-3330">Maiesties</w>
     <w lemma="officer" pos="n2" xml:id="A22317-001-a-3340">Officers</w>
     <pc xml:id="A22317-001-a-3350">,</pc>
     <w lemma="minister" pos="n2" xml:id="A22317-001-a-3360">Ministers</w>
     <w lemma="and" pos="cc" xml:id="A22317-001-a-3370">and</w>
     <w lemma="subject" pos="n2" reg="Subjects" xml:id="A22317-001-a-3380">Subiects</w>
     <pc xml:id="A22317-001-a-3390">,</pc>
     <w lemma="that" pos="cs" xml:id="A22317-001-a-3400">That</w>
     <w lemma="they" pos="pns" xml:id="A22317-001-a-3410">they</w>
     <pc xml:id="A22317-001-a-3420">,</pc>
     <w lemma="and" pos="cc" xml:id="A22317-001-a-3430">and</w>
     <w lemma="every" pos="d" reg="every" xml:id="A22317-001-a-3440">euery</w>
     <w lemma="of" pos="acp" xml:id="A22317-001-a-3450">of</w>
     <w lemma="they" pos="pno" xml:id="A22317-001-a-3460">them</w>
     <w lemma="in" pos="acp" xml:id="A22317-001-a-3470">in</w>
     <w lemma="their" pos="po" xml:id="A22317-001-a-3480">their</w>
     <w lemma="several" pos="j" reg="several" xml:id="A22317-001-a-3490">seuerall</w>
     <w lemma="place" pos="n2" xml:id="A22317-001-a-3500">places</w>
     <pc xml:id="A22317-001-a-3510">,</pc>
     <w lemma="do" pos="vvb" reg="do" xml:id="A22317-001-a-3520">doe</w>
     <w lemma="not" pos="xx" xml:id="A22317-001-a-3530">not</w>
     <w lemma="only" pos="av-j" reg="only" xml:id="A22317-001-a-3540">onely</w>
     <w lemma="use" pos="vvi" reg="use" xml:id="A22317-001-a-3550">vse</w>
     <w lemma="their" pos="po" xml:id="A22317-001-a-3560">their</w>
     <w lemma="best" pos="js" xml:id="A22317-001-a-3570">best</w>
     <w lemma="endeavour" pos="n1" reg="endeavour" xml:id="A22317-001-a-3580">endeauour</w>
     <w lemma="to" pos="prt" xml:id="A22317-001-a-3590">to</w>
     <w lemma="prevent" pos="vvi" reg="prevent" xml:id="A22317-001-a-3600">preuent</w>
     <w lemma="and" pos="cc" xml:id="A22317-001-a-3610">and</w>
     <w lemma="restrain" pos="vvi" reg="restrain" xml:id="A22317-001-a-3620">restraine</w>
     <w lemma="all" pos="d" xml:id="A22317-001-a-3630">all</w>
     <w lemma="insolence" pos="n2" reg="insolences" xml:id="A22317-001-a-3640">insolencies</w>
     <pc xml:id="A22317-001-a-3650">,</pc>
     <w lemma="misbehaviour" pos="n2" reg="misbehaviours" xml:id="A22317-001-a-3660">misbehauiours</w>
     <pc xml:id="A22317-001-a-3670">,</pc>
     <w lemma="and" pos="cc" xml:id="A22317-001-a-3680">and</w>
     <w lemma="offence" pos="n2" xml:id="A22317-001-a-3690">offences</w>
     <w lemma="aforementioned" pos="j" xml:id="A22317-001-a-3700">afore-mentioned</w>
     <pc xml:id="A22317-001-a-3710">,</pc>
     <w lemma="but" pos="acp" xml:id="A22317-001-a-3720">but</w>
     <w lemma="also" pos="av" xml:id="A22317-001-a-3730">also</w>
     <w lemma="take" pos="vvb" xml:id="A22317-001-a-3740">take</w>
     <w lemma="order" pos="n1" xml:id="A22317-001-a-3750">order</w>
     <w lemma="that" pos="cs" xml:id="A22317-001-a-3760">that</w>
     <w lemma="all" pos="d" xml:id="A22317-001-a-3770">all</w>
     <w lemma="offender" pos="n2" xml:id="A22317-001-a-3780">offenders</w>
     <w lemma="therein" pos="av" xml:id="A22317-001-a-3790">therein</w>
     <w lemma="may" pos="vmb" xml:id="A22317-001-a-3800">may</w>
     <w lemma="receive" pos="vvi" reg="receive" xml:id="A22317-001-a-3810">receiue</w>
     <w lemma="speedy" pos="j" reg="speedy" xml:id="A22317-001-a-3820">speedie</w>
     <w lemma="and" pos="cc" xml:id="A22317-001-a-3830">and</w>
     <w lemma="condign" pos="j" reg="condign" xml:id="A22317-001-a-3840">condigne</w>
     <w lemma="punishment" pos="n1" xml:id="A22317-001-a-3850">punishment</w>
     <w lemma="according" pos="j" xml:id="A22317-001-a-3860">according</w>
     <w lemma="to" pos="acp" xml:id="A22317-001-a-3870">to</w>
     <w lemma="their" pos="po" xml:id="A22317-001-a-3880">their</w>
     <w lemma="demerit" pos="n2" xml:id="A22317-001-a-3890">demerits</w>
     <pc xml:id="A22317-001-a-3900">,</pc>
     <w lemma="to" pos="prt" xml:id="A22317-001-a-3910">to</w>
     <w lemma="the" pos="d" xml:id="A22317-001-a-3920">the</w>
     <w lemma="terror" pos="n1" xml:id="A22317-001-a-3930">terror</w>
     <w lemma="and" pos="cc" xml:id="A22317-001-a-3940">and</w>
     <w lemma="example" pos="n1" xml:id="A22317-001-a-3950">example</w>
     <w lemma="of" pos="acp" xml:id="A22317-001-a-3960">of</w>
     <w lemma="other" pos="pi2-d" xml:id="A22317-001-a-3970">others</w>
     <pc unit="sentence" xml:id="A22317-001-a-3980">.</pc>
    </p>
    <closer xml:id="A22317-e10140">
     <dateline xml:id="A22317-e10150">
      <w lemma="give" pos="vvn" reg="Given" xml:id="A22317-001-a-3990">Giuen</w>
      <w lemma="at" pos="acp" xml:id="A22317-001-a-4000">at</w>
      <w lemma="our" pos="po" xml:id="A22317-001-a-4010">Our</w>
      <w lemma="court" pos="n1" xml:id="A22317-001-a-4020">Court</w>
      <w lemma="at" pos="acp" xml:id="A22317-001-a-4030">at</w>
      <w lemma="Whitehall" pos="nn1" xml:id="A22317-001-a-4040">Whitehall</w>
      <pc xml:id="A22317-001-a-4050">,</pc>
      <date xml:id="A22317-e10160">
       <w lemma="the" pos="d" xml:id="A22317-001-a-4060">the</w>
       <w lemma="eight" pos="crd" xml:id="A22317-001-a-4070">eight</w>
       <w lemma="day" pos="n1" xml:id="A22317-001-a-4080">day</w>
       <w lemma="of" pos="acp" xml:id="A22317-001-a-4090">of</w>
       <w lemma="march" pos="n1" xml:id="A22317-001-a-4100">March</w>
       <pc xml:id="A22317-001-a-4110">,</pc>
       <w lemma="in" pos="acp" xml:id="A22317-001-a-4120">in</w>
       <w lemma="the" pos="d" xml:id="A22317-001-a-4130">the</w>
       <w lemma="one" pos="crd" xml:id="A22317-001-a-4140">one</w>
       <w lemma="and" pos="cc" xml:id="A22317-001-a-4150">and</w>
       <w lemma="twenty" pos="ord" xml:id="A22317-001-a-4160">twentieth</w>
       <w lemma="yeereof" pos="av" xml:id="A22317-001-a-4170">yeereof</w>
       <w lemma="our" pos="po" xml:id="A22317-001-a-4180">Our</w>
       <w lemma="reign" pos="n1" reg="Reign" xml:id="A22317-001-a-4190">Reigne</w>
       <w lemma="of" pos="acp" xml:id="A22317-001-a-4200">of</w>
       <w lemma="great" pos="j" xml:id="A22317-001-a-4210">Great</w>
       <w lemma="Britain" pos="nn1" reg="Britain" xml:id="A22317-001-a-4220">Britaine</w>
       <pc xml:id="A22317-001-a-4230">,</pc>
       <w lemma="France" pos="nn1" xml:id="A22317-001-a-4240">France</w>
       <w lemma="and" pos="cc" xml:id="A22317-001-a-4250">and</w>
       <w lemma="Ireland" pos="nn1" xml:id="A22317-001-a-4260">Ireland</w>
       <pc unit="sentence" xml:id="A22317-001-a-4270">.</pc>
      </date>
     </dateline>
     <lb xml:id="A22317-e10170"/>
     <w lemma="God" pos="nn1" xml:id="A22317-001-a-4280">God</w>
     <w lemma="save" pos="acp" reg="save" xml:id="A22317-001-a-4290">saue</w>
     <w lemma="the" pos="d" xml:id="A22317-001-a-4300">the</w>
     <w lemma="King" pos="n1" xml:id="A22317-001-a-4310">King</w>
     <pc unit="sentence" xml:id="A22317-001-a-4311">.</pc>
    </closer>
   </div>
  </body>
  <back xml:id="A22317-e10180">
   <div type="colophon" xml:id="A22317-e10190">
    <p xml:id="A22317-e10200">
     <w lemma="imprint" pos="vvn" xml:id="A22317-001-a-4330">Imprinted</w>
     <w lemma="at" pos="acp" xml:id="A22317-001-a-4340">at</w>
     <w lemma="London" pos="nn1" xml:id="A22317-001-a-4350">London</w>
     <w lemma="by" pos="acp" xml:id="A22317-001-a-4360">by</w>
     <w lemma="bonham" pos="nn1" xml:id="A22317-001-a-4370">Bonham</w>
     <w lemma="Norton" pos="nn1" xml:id="A22317-001-a-4380">Norton</w>
     <w lemma="and" pos="cc" xml:id="A22317-001-a-4390">and</w>
     <w lemma="John" pos="nn1" reg="john" xml:id="A22317-001-a-4400">Iohn</w>
     <w lemma="bill" pos="n1" xml:id="A22317-001-a-4410">Bill</w>
     <pc xml:id="A22317-001-a-4420">,</pc>
     <w lemma="printer" pos="n2" xml:id="A22317-001-a-4430">Printers</w>
     <w lemma="to" pos="acp" xml:id="A22317-001-a-4440">to</w>
     <w lemma="the" pos="d" xml:id="A22317-001-a-4450">the</w>
     <w lemma="king" pos="n2" xml:id="A22317-001-a-4460">Kings</w>
     <w lemma="most" pos="avs-d" xml:id="A22317-001-a-4470">most</w>
     <w lemma="excellent" pos="j" xml:id="A22317-001-a-4480">Excellent</w>
     <w lemma="majesty" pos="n1" reg="Majesty" xml:id="A22317-001-a-4490">Maiestie</w>
     <pc unit="sentence" xml:id="A22317-001-a-4500">.</pc>
     <w lemma="1623" pos="crd" xml:id="A22317-001-a-4510">M.DC.XXIII</w>
     <pc unit="sentence" xml:id="A22317-001-a-4520">.</pc>
    </p>
   </div>
  </back>
 </text>
</TEI>
